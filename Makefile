
ejemplo.out: Contador.c DLL.c main.c
	gcc -Wall -std=c99 -o ejemplo.out Contador.c DLL.c main.c

.PHONY: clean

clean:
	rm -f ejemplo.out *.~ *~
