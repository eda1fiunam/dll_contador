
//
// Compilación:
// gcc -Wall -std=c99 -o prueba.out main.c Contador.c DLL.c
//



#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "bool.h"
#include "Contador.h"
#include "DLL.h"




int main(void)
{
	DLL* lista = DLL_New();
	// creamos a la lista
	
	Contador* c = Contador_New( 10 );
	// creamos a un objeto Contador. Lo más natural es que esta
	// llamada esté en otra función
	
	// usamos a c: terminamos de llenarlo, lo imprimimos, etc.

	DLL_InsertBack( lista, c );
	// insertamos a c en la lista



	Contador* x = DLL_RemoveBack( lista );
	// extraemos a un objeto Contador. Lo más natural es que esta llamada
	// esté en otra función
	
	// usamos a x: lo imprimimos, etc.
	
	Contador_Delete( x );
	// devolvemos la memoria del objeto x
	

	DLL_Delete( lista );
	// destruímos a la lista cuando hayamos terminado con ella. Lo más
	// natural es que esta llamada esté en otra función
	return 0;
}
