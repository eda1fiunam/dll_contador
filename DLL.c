/*Copyright (C) 
 * 2018 - eda1 dot fiunam at yahoo dot com dot mx
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#include "DLL.h"


/*-------------------------------------------------------------------
 *  funciones privadas!!
 *-----------------------------------------------------------------*/

static Node* newNode( Contador* c )
{
	assert( c != NULL );
	//    ó
	// crea al objeto Contador
	//    ó
	// return NULL;

	Node* n = ( Node* ) malloc( sizeof ( Node ) );
	if( n != NULL ) {
		n->info = c;
		n->next = NULL;
		n->prev = NULL;
	}

	return n;
}

/*-------------------------------------------------------------------
 *  Funciones públicas (operaciones) !!
 *-----------------------------------------------------------------*/

/**
 * @brief Crea una nueva lista
 *
 * @return Referencia a la nueva lista; NULL en caso de error
 */
DLL* DLL_New()
{
	DLL* p = (DLL*) malloc (sizeof (DLL));
	if( p != NULL ) {
		p->first = NULL;
		p->last = NULL;
	}

	return p;
}

/**
 * @brief Destruye una lista
 *
 * @param this Referencia al objeto que se quiere destruir
 */
void DLL_Delete( DLL* this )
{
	assert( this );

	while( this->first != NULL ) {
		Node* tmp = this->first->next;
		free( this->first );
		this->first = tmp;

		this->last->next = this->first;
		// tal vez no se necesita
	}

	free( this );
	this = NULL;
}

/**
 * @brief Inserta un elemento en la parte posterior de la lista
 *
 * @param this Referencia a un objeto tipo DLL
 * @param c Es el elemento que se desea insertar en la lista
 *
 * @return false si hubo un problema asignando memoria para el nuevo nodo; true
 *         en caso contrario
 */
bool DLL_InsertBack( DLL* this, Contador* c )
{
	assert (this);

	Node* n = newNode( c );
	if( n == NULL ) { return false;	}

	if( this->first == NULL ){
		this->first = n;
		this->last = n;
	}
	else {
		this->last->next = n;
		n->prev = this->last;
		this->last = n;
	}

	return true;
}

/**
 * @brief Inserta un elemento en la parte frontal de la lista
 *
 * @param this Referencia a un objeto tipo DLL
 * @param c Es el elemento que se desea insertar en la lista
 *
 * @return false si hubo un problema asignando memoria para el nuevo nodo; true
 *         en caso contrario
 */
bool DLL_InsertFront( DLL* this, Contador* c )
{
	assert( this );

	Node* n = newNode( c );
	if( n == NULL ) { return false;	}

	if( this->first == NULL ){
		this->first = n;
		this->last = n;
	}
	else {
		n->next = this->first;
		this->first->prev = n;
		this->first = n;
	}

	return true;
}


/**
 * @brief Extrae el elemento en la parte frontal de la lista
 *
 * @param this Referencia a un objeto tipo DLL
 *
 * @return Una referencia al elemento en el frente de la lista
 *
 * @pre La lista existe; en caso contrario se dispara una aserción
 * @pre La lista no está vacía; en caso contrario se dispara una aserción
 */
Contador* DLL_RemoveFront( DLL* this )
{
	assert( this );
	assert( this->first != NULL );

	Contador* c = this->first->info;

	if( this->first == this->last ){
		free( this->first );
		this->first = NULL;
		this->last = NULL;
	}
	else{
		Node* tmp = this->first->next;
		free( this->first );
		this->first = tmp;
		this->first->prev = NULL;
	}

	return c;

}

/**
 * @brief Extrae el elemento en la parte posterior de la lista
 *
 * @param this Referencia a un objeto tipo DLL
 *
 * @return Una referencia al elemento en el fondo de la lista
 *
 * @pre La lista existe; en caso contrario se dispara una aserción
 * @pre La lista no está vacía; en caso contrario se dispara una aserción
 */
Contador* DLL_RemoveBack( DLL* this )
{
	assert( this );

	assert( this->first != NULL );

	Contador* c = this->last->info;

	Node* it = this->first;
	// NO PODEMOS PERDER A FIRST !!!

	while( it->next != this->last ){
		Node* tmp = it->next;
		it = tmp;
	}

	it->next = NULL;
	free( this->last );
	this->last = it;

	return c;
}

/**
 * @brief Indica si una lista está vacía o no
 *
 * @param this Referencia a un objeto tipo DLL
 *
 * @return true si la lista está vacía; false en caso contrario
 */
bool DLL_IsEmpty (DLL* this)
{
	assert( this );

	return this->first == NULL ? true : false;
}

/**
 * @brief Purga (limpia) una lista. 
 * Esta función no elimina la lista. Si lo que se desea es eliminarla,
 * entonces se debe usar la función \see DLL_Delete()
 *
 * @param this Referencia a un objeto tipo DLL
 */
void DLL_Purge( DLL* this )
{
	assert( this );

	if( DLL_IsEmpty( this ) ) { return; }

	while( this->first != NULL ){
		Node* tmp = this->first->next;
		free( this->first );
		this->first = tmp;
	}
}

/**
 * @brief Indica si un elemento está en la lista
 *
 * @param this Referencia a un objeto tipo DLL
 * @param c Elemento que se desea buscar
 *
 * @return true si el elemento ya está en la lista; false en caso contrario
 */
bool DLL_FindIf( DLL* this, const Contador* c )
{
	assert( this );

	Node* it = this->first;
	// NO PODEMOS PERDER A FIRST !!!
	
	while( it != NULL ) {
		if( Contador_IsEqual( it->info, c )){
			return true;
		}
		else {
			// es sumamente peligroso hacer lo siguiente:
			// it = it->next
			// por eso lo hemos hecho en dos pasos

			Node* tmp = it->next;
			it = tmp;
		}
	}

	return false;
}

/**
 * @brief Coloca al cursor en el primer elemento de la lista
 *
 * @param this Referencia a un objeto tipo DLL
 */
void DLL_First( DLL* this )
{
	this->cursor = this->first;
}

/**
 * @brief Coloca al cursor en el último elemento de la lista
 *
 * @param this Referencia a un objeto tipo DLL
 */
void DLL_Last( DLL* this )
{
	this->cursor = this->last;
}

/**
 * @brief Desplaza el cursor una posición a la derecha
 *
 * @param this Referencia a un objeto tipo DLL
 *
 * @pre No hace nada cuando el cursor está completamente a la derecha, e.g. ya
 * es NULL
 */
void DLL_Next( DLL* this )
{
	if( this->cursor != NULL ){
		Node* tmp = this->cursor->next;
		this->cursor = tmp;
	}
}

/**
 * @brief Desplaza el cursor una posición a la izquierda
 *
 * @param this Referencia a un objeto tipo DLL
 *
 * @pre No hace nada cuando el cursor está completamente a la izquierda, e.g. ya
 * es NULL
 */
void DLL_Prev( DLL* this )
{
	if( this->cursor != NULL ){
		Node* tmp = this->cursor->prev;
		this->cursor = tmp;
	}
}
