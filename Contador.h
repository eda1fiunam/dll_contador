/*Copyright (C) 
 * 2018 - eda1 dot fiunam at yahoo dot com dot mx
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

/**
 * @file Contador.h
 * @brief TDU para un contador ascendente
 * @author eda1 dot fiunam at yahoo dot com dot mx
 * @version 0.1
 * @date 2018-03-08
 */


#ifndef  CONTADOR_INC
#define  CONTADOR_INC


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "bool.h"

typedef struct
{
	size_t max;
	size_t cuenta;
} Contador;

Contador* Contador_New( size_t _max );
void Contador_Delete( Contador* this );
void Contador_Incrementa( Contador* this );
void Contador_Reset( Contador* this );
size_t Contador_GetCuenta( const Contador* this );
size_t Contador_GetMax( const Contador* this );
void Contador_SetMax( Contador* this, size_t _maximo );

//
// Cualquier TDU que escribamos debería incorporar las siguientes funciones. La
// semántica de cada una de ellas va a depender de la naturaleza del objeto que
// el TDU esté intentando modelar.
//

void Contador_Copy( Contador* this, const Contador* other );
bool Contador_IsEqual( const Contador* this, const Contador* other );
bool Contador_IsDifferent( const Contador* this, const Contador* other );
bool Contador_IsGreater( const Contador* this, const Contador* other );
bool Contador_IsEqualOrGreater( const Contador* this, const Contador* other );
void Contador_Swap( Contador* this, Contador* other );


#endif   /* ----- #ifndef CONTADOR_INC  ----- */
