/*Copyright (C) 
 * 2018 - eda1 dot fiunam at yahoo dot com dot mx
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include "Contador.h"

/**
 * @brief Incrementa al contador en una unidad.
 *
 * @param this Referencia al objeto de trabajo
 *
 * @post El contador hace un rollover cuando se alcanza el límite máximo menos
 * uno
 */
void Contador_Incrementa( Contador* this )
{
	++this->cuenta;
	if( this->cuenta >= this->max ) {
		this->cuenta = 0;
	}
}

/**
 * @brief Pone a cero la cuenta corriente
 *
 * @param this Referencia al objeto de trabajo
 */
void Contador_Reset( Contador* this )
{
	this->cuenta = 0;
}

/**
 * @brief Devuelve el valor de la cuenta corriente
 *
 * @param this Referencia al objeto de trabajo
 *
 * @return La cuenta corriente
 */
size_t Contador_GetCuenta( const Contador* this )
{
	assert( this );

	return this->cuenta;
}

/*--------------------------------------------------------------------------*/
/**
 * @brief Devuelve el valor de la cuenta máxima
 *
 * @param this Referencia al objeto de trabajo
 *
 * @return La cuenta máxima
 */
size_t Contador_GetMax( const Contador* this )
{
	assert( this );

	return this->max;
}

/**
 * @brief Establece la cuenta máxima del contador.
 *
 * @param this Referencia al objeto de trabajo
 * @param _maximo Es la cuenta máxima.
 */
void Contador_SetMax( Contador* this, size_t _maximo )
{
	assert( this );

	this->max = _maximo;
}


/**
 * @brief Crea a un nuevo objeto Contador
 *
 * @param _max La cuenta máxima del contador
 *
 * @return Una referencia al objeto creado. NULL en caso de error asignando la
 * memoria
 */
Contador* Contador_New( size_t _max )
{
	Contador* c = (Contador*) malloc( sizeof( Contador ) );
	if( c != NULL ){
		c->max = _max;
		c->cuenta = 0;
	}

	return c;
}

/**
 * @brief Destruye a un objeto Contador
 *
 * @param this Referencia al objeto a destruir
 *
 * @pre El objeto debe existir
 * @post La referencia al objeto destruído se pone a NULL
 */
void Contador_Delete( Contador* this )
{
	assert( this );

	free( this );
	this = NULL;
}

/**
 * @brief Copia (asigna) un objeto a otro
 *
 * @param this El objeto destino
 * @param other El objeto fuente
 */
void Contador_Copy( Contador* this, const Contador* other )
{
	assert( this );
	assert( other );

	this->max = other->max;
	this->cuenta = other->cuenta;
}

/**
 * @brief Compara si dos objetos son iguales
 * 
 * Previamente se debe definir qué significa que dos objetos sean iguales, y
 * posteriormente documentar tal decisión.
 *
 * @param this El primer objeto a comparar
 * @param other El segundo objeto a comparar
 *
 * @return true si los objetos son iguales?
 * false en caso contrario
 */
bool Contador_IsEqual( const Contador* this, const Contador* other )
{
	assert( this );
	assert( other );

	return this->cuenta == other->cuenta;
}

/**
 * @brief Compara si un objeto es mayor que otro
 *
 * @param this El primer objeto a comparar (lado izquierdo)
 * @param other El segundo objeto a comparar (lado derecho)
 *
 * @return true si los objetos son iguales?\n
 * false en caso contrario
 */
bool Contador_IsGreater( const Contador* this, const Contador* other )
{
	assert( this );
	assert( other );

	return this->cuenta > other->cuenta;
}

/**
 * @brief Compara si dos objetos son diferentes
 * 
 * Previamente se debe definir qué significa que dos objetos sean diferentes, y
 * posteriormente documentar tal decisión.
 *
 * @param this El primer objeto a comparar
 * @param other El segundo objeto a comparar
 *
 * @return true si los objetos son diferentes
 * false en caso contrario
 */
bool Contador_IsDifferent( const Contador* this, const Contador* other )
{
	return !Contador_IsEqual( this, other );
}

/**
 * @brief Compara si un objeto es igual o mayor que otro
 *
 * @param this El primer objeto a comparar (lado izquierdo)
 * @param other El segundo objeto a comparar (lado derecho)
 *
 * @return true si el objeto this es igual o mayor que el objeto other
 * false en caso contrario
 */
bool Contador_IsEqualOrGreater( const Contador* this, const Contador* other )
{
	return ( Contador_IsEqual( this, other ) || Contador_IsGreater( this, other ) );
}

/**
 * @brief Intercambia dos objetos
 *
 * @param this El primero de los objetos a intercambiar
 * @param other El segundo de los objetos a intercambiar
 *
 * @pre Ambos objetos deben existir
 */
void Contador_Swap( Contador* this, Contador* other )
{

	assert( this );
	assert( other );

	Contador tmp;
	tmp.max = this->max;
	this->max = other->max;
	other->max = tmp.max;

	tmp.cuenta = this->cuenta;
	this->cuenta = other->cuenta;
	other->cuenta = tmp.cuenta;
}
